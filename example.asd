;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: asdf -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          example.asd
;;;; Project:       Example
;;;; Purpose:       System definition file for complete application
;;;; Programmer:    David Steuber
;;;; Date Started:  5/2/2005
;;;;
;;;; $Id: $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :cl-user)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defpackage :maker
    (:use common-lisp ccl)
    (:import-from :ccl)))

(in-package :maker)

(defparameter load-success nil)

(require :asdf)

(in-package #:asdf)

(asdf:defsystem #:example
  :version "0.1"
  :serial t
  :depends-on ("cl-carbon")
  :components
  ((:module src
            :serial t
            :components
            ((:file "package")
             (:file "main")))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (multiple-value-bind (r c)
      (ignore-errors
        (operate 'load-op :example)
        (setf maker::load-success t))
    (when c (format t "Condition signaled: < ~A >~%" c))
    (if maker::load-success
        (progn (format t "System loaded...~%") (force-output))
        (progn
          (format t "System load failed!~%") 
          (force-output)
          (defpackage :example)))))

(in-package :maker)

(defun make (&key application-name)
  (declare (special load-success))
  (if load-success
      (let ((image-name (concatenate 'string application-name ".image"))
            (lisp-kernel-path (car ccl::*command-line-argument-list*))
            (lcode-kernel-path (concatenate 'string (ccl::current-directory-name) "/" application-name)))
        (format t "Saving application ~S...~%" application-name)
        (force-output)
        (ccl::copy-file lisp-kernel-path lcode-kernel-path :if-exists :supersede)
        (ccl::save-application image-name :toplevel-function #'example::main))
      (quit)))

;(ccl::init-logical-directories)

