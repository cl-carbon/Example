;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10; Package: EXAMPLE -*-
;;;; ***********************************************************************
;;;; FILE IDENTIFICATION
;;;;
;;;; Name:          main.lisp
;;;; Project:       Example
;;;; Purpose:       "main" function goes here like a C program
;;;; Programmer:    David Steuber
;;;; Date Started:  5/2/2005
;;;;
;;;; $Id: $
;;;; ***********************************************************************
;;;;
;;;; Copyright (c) 2005 by David Steuber
;;;; 
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;; 
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;; 
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;;; 
;;;; ***********************************************************************

(in-package :example)

(defclass example-application (carbon:application)
  ()
  (:documentation "An example application"))

(defun make-example-application ()
  (make-instance 'example-application))

(defmethod initialize-instance :after ((app example-application) &rest initargs)
  (declare (ignore initargs))
  (rlet ((nibref :<ibn>ib<r>ef))
    (carbon:require-noerror
      (#_CreateNibReference (carbon:const-cfstring "main") nibref)
      (#_SetMenuBarFromNib (ccl::%get-ptr nibref) (carbon:const-cfstring "MenuBar")))
    (#_DisposeNibReference (ccl::%get-ptr nibref))))

(defmethod carbon:menu-command ((app example-application) command)
  (declare (ignore command))
  (carbon:show-alert "This menu command not implemented.  Allowing default processing.")
  nil)

(defun main ()
  (carbon:run-application (make-example-application)))
