#!/bin/sh
#

application_name="Example"
bundle_path="./bin/${application_name}.app"
system_definition="./example.asd"

echo "Removing stale dfsl files..."
find . -name '*.dfsl' -exec rm {} \;

echo "Building  Example..."

make_form="(maker::make :application-name \"${application_name}\")"

openmcl -l ${system_definition} -e "${make_form}"

mv "${application_name}" "${bundle_path}/Contents/MacOS"
mv "${application_name}.image" "${bundle_path}/Contents/MacOS"
touch "${bundle_path}"

echo
echo "Done"
echo
